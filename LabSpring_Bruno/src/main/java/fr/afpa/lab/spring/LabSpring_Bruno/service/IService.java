package fr.afpa.lab.spring.LabSpring_Bruno.service;

import java.util.HashMap;

public interface IService<T> {

	public void afficherListe();
	
	public T afficherByID(Integer id);
	
	public HashMap<Integer, T> getMap();

	public T getByID(Integer id, HashMap<Integer, T> hashMap);
	
}
