package fr.afpa.lab.spring.LabSpring_Bruno.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.lab.spring.LabSpring_Bruno.beans.Voiture;
import fr.afpa.lab.spring.LabSpring_Bruno.dao.DaoVoiture;

@Service("serviceVoiture")
public class ServiceVoiture implements IService<Voiture>{

	@Autowired
	private DaoVoiture daoVoiture;

	public void afficherListe() {
		List<Voiture> listeVoiture = daoVoiture.getListeVoiture();
		for(Voiture voiture : listeVoiture) {
			System.out.println(voiture.toString());
		}
	}

	public HashMap<Integer, Voiture> getMap() {
		List<Voiture> listeVoiture = daoVoiture.getListeVoiture();
		HashMap<Integer, Voiture> mapVoiture = new HashMap<>();
		for(Voiture voiture : listeVoiture) {
			mapVoiture.put(voiture.getId(), voiture);
		}
		return mapVoiture;
	}

	public Voiture getByID(Integer id, HashMap<Integer, Voiture> mapVoiture) {
		return mapVoiture.get(id);
	}

	public Voiture afficherByID(Integer id) {
		Voiture voiture = getByID(id, getMap());
		return voiture;
	}



}
