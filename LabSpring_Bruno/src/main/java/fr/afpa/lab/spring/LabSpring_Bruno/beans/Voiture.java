package fr.afpa.lab.spring.LabSpring_Bruno.beans;

public class Voiture {

	Integer id;
	String nom;
	String marque;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getMarque() {
		return marque;
	}
	public void setMarque(String marque) {
		this.marque = marque;
	}
	
	@Override
	public String toString() {
		return "Voiture [id=" + id + ", nom=" + nom + ", marque=" + marque + "]";
	}
}
