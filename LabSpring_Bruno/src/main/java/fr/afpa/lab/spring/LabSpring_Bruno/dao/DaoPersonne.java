package fr.afpa.lab.spring.LabSpring_Bruno.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.afpa.lab.spring.LabSpring_Bruno.beans.Personne;

@Repository
public class DaoPersonne {
	
	List<Personne> listePersonne = new ArrayList<Personne>();
	Personne bobMarley = new Personne();
	Personne britneySpears = new Personne();
	Personne theRock = new Personne();

	public List<Personne> getListePersonne() {
		listePersonne.add(bobMarley);
		listePersonne.add(britneySpears);
		listePersonne.add(theRock);
		
		bobMarley.setId(1);
		bobMarley.setNom("Marley");
		bobMarley.setPrenom("Bob");
		bobMarley.setAge(36);
		
		britneySpears.setId(2);
		britneySpears.setNom("Spears");
		britneySpears.setPrenom("Britney");
		britneySpears.setAge(39);

		theRock.setId(3);
		theRock.setNom("Johnson");
		theRock.setPrenom("Dwayne");
		theRock.setAge(48);

		return listePersonne;
	}
}