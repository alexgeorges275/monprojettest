package fr.afpa.lab.spring.LabSpring_Bruno.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.lab.spring.LabSpring_Bruno.beans.Ville;
import fr.afpa.lab.spring.LabSpring_Bruno.dao.DaoVille;

@Service("serviceVille")
public class ServiceVille implements IService<Ville>{

	@Autowired
	private DaoVille daoVille;

	public void afficherListe() {
		List<Ville> listeVille = daoVille.getListeVille();
		for(Ville ville : listeVille) {
			System.out.println(ville.toString());
		}
	}

	public HashMap<Integer, Ville> getMap() {
		List<Ville> listeVille = daoVille.getListeVille();
		HashMap<Integer, Ville> mapVille = new HashMap<>();
		for(Ville ville : listeVille) {
			mapVille.put(ville.getId(), ville);
		}
		return mapVille;
	}

	public Ville getByID(Integer id, HashMap<Integer, Ville> mapVille) {
		return mapVille.get(id);
	}

	public Ville afficherByID(Integer id) {
		Ville ville = getByID(id, getMap());
		return ville;
	}



}
