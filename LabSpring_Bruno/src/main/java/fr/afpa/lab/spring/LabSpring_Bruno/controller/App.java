package fr.afpa.lab.spring.LabSpring_Bruno.controller;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.afpa.lab.spring.LabSpring_Bruno.beans.Personne;
import fr.afpa.lab.spring.LabSpring_Bruno.beans.Ville;
import fr.afpa.lab.spring.LabSpring_Bruno.beans.Voiture;
import fr.afpa.lab.spring.LabSpring_Bruno.config.AppConfiguration;
import fr.afpa.lab.spring.LabSpring_Bruno.service.IService;
import fr.afpa.lab.spring.LabSpring_Bruno.service.ServiceVoiture;
import fr.afpa.lab.spring.LabSpring_Bruno.service.Service2;
import fr.afpa.lab.spring.LabSpring_Bruno.service.ServiceExterne;
import fr.afpa.lab.spring.LabSpring_Bruno.service.ServicePersonne;
import fr.afpa.lab.spring.LabSpring_Bruno.service.ServiceVille;

/**
 * Hello world!
 *
 */

public class App 
{
    public static void main( String[] args )
    {
        System.out.println("========== Début de l'application ==========\n");

		// Récupération du contexte
		@SuppressWarnings("resource")
		ApplicationContext context= new AnnotationConfigApplicationContext(AppConfiguration.class);

		// Appelle mon service externe
		ServiceExterne serviceExterne = (ServiceExterne) context.getBean("servExt");
		System.out.println(serviceExterne.bonjour());
		

		
		// ========== PERSONNES ==========
		IService<Personne> servicePersonne = (ServicePersonne) context.getBean("servicePersonne");

		// Afficher la liste des personnes
		System.out.println("\nListe des personnes : ");
		servicePersonne.afficherListe();
		
		// Affiche une personne par son ID
		Integer id = 3;
		System.out.println("Personne avec l'id = "+id);
		Personne theRock = (Personne) servicePersonne.afficherByID(id);
		System.out.println(theRock.getPrenom()+" "+theRock.getNom());

		
		
		// ========== VILLES ==========
		IService<Ville> serviceVille = (ServiceVille) context.getBean("serviceVille");
		
		// Afficher la liste des villes
		System.out.println("\nListe des villes : ");
		serviceVille.afficherListe();
		
		// Affiche une ville par son ID
		Integer id2 = 2;
		System.out.println("Ville avec l'id = "+id2);
		System.out.println(( (Ville) (serviceVille.afficherByID(id2)) ).getNom());
		
		
		
		// ========== VOITURES ==========
		IService<Voiture> serviceVoiture = (ServiceVoiture) context.getBean("serviceVoiture");
		
		// Afficher la liste de mes voitures
		System.out.println("\nListe des voitures : ");
		serviceVoiture.afficherListe();
		
		// Affiche une voiture par son ID
		Integer id3 = 1;
		System.out.println("Voiture avec l'id = "+id3);
		System.out.println(( (Voiture) (serviceVoiture.afficherByID(id3)) ).getNom());
		


		// Appelle mon service 2
		Service2 service2 = (Service2) context.getBean("service2");
		service2.process();
		
    }
}
