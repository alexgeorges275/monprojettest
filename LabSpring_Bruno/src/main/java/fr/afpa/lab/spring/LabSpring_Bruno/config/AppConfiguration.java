package fr.afpa.lab.spring.LabSpring_Bruno.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;

import fr.afpa.lab.spring.LabSpring_Bruno.service.ServiceExterne;

//@Configuration
//Indiquer à Spring qu'il doit chercher les Beans et s'occuper des instances
@ComponentScan("fr.afpa.lab.spring.LabSpring_Bruno")
public class AppConfiguration {
	
	@Bean
	ServiceExterne servExt() {
		return new ServiceExterne();
	}
	
//	@Bean
//	Service1 service1() {
//		return new Service1();
//	}

}
