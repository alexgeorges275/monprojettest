package fr.afpa.lab.spring.LabSpring_Bruno.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.afpa.lab.spring.LabSpring_Bruno.beans.Voiture;

@Repository
public class DaoVoiture {

	List<Voiture> listeVoiture = new ArrayList<Voiture>();
	Voiture peugeot206 = new Voiture();
	Voiture golf8 = new Voiture();
	Voiture ferrariEnzo = new Voiture();
	
	
	
	public List<Voiture> getListeVoiture() {
		listeVoiture.add(peugeot206);
		listeVoiture.add(golf8);
		listeVoiture.add(ferrariEnzo);

		peugeot206.setId(1);
		peugeot206.setNom("Peugeot 206");
		peugeot206.setMarque("Peugeot");
		
		golf8.setId(2);
		golf8.setNom("Golf 8");
		golf8.setMarque("Volkswagen");

		ferrariEnzo.setId(3);
		ferrariEnzo.setNom("Ferrari Enzo");
		ferrariEnzo.setMarque("Ferrari");

//		System.out.println(peugeot206.toString());
//		System.out.println(golf8.toString());
//		System.out.println(ferrariEnzo.toString());
		
		return listeVoiture;
	}
}