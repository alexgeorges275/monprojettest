package fr.afpa.lab.spring.LabSpring_Bruno.beans;

public class Ville {

	Integer id;
	String nom;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@Override
	public String toString() {
		return "Ville [id=" + id + ", nom=" + nom + "]";
	}
}
