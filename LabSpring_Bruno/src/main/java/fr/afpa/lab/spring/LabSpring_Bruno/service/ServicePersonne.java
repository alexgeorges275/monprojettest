package fr.afpa.lab.spring.LabSpring_Bruno.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.lab.spring.LabSpring_Bruno.beans.Personne;
import fr.afpa.lab.spring.LabSpring_Bruno.dao.DaoPersonne;

@Service("servicePersonne")
public class ServicePersonne implements IService<Personne>{

	@Autowired
	private DaoPersonne daoPersonne;

	public void afficherListe() {
		List<Personne> listePersonne = daoPersonne.getListePersonne();
		for(Personne personne : listePersonne) {
			System.out.println(personne.toString());
		}
	}

	public HashMap<Integer, Personne> getMap() {
		List<Personne> listePersonne = daoPersonne.getListePersonne();
		HashMap<Integer, Personne> mapPersonne = new HashMap<>();
		for(Personne personne : listePersonne) {
			mapPersonne.put(personne.getId(), personne);
		}
		return mapPersonne;
	}


	public Personne getByID(Integer id, HashMap<Integer, Personne> mapPersonne) {
		return mapPersonne.get(id);
	}
	
	public Personne afficherByID(Integer id) {
		Personne personne = getByID(id, getMap());
		return personne;
	}




}
