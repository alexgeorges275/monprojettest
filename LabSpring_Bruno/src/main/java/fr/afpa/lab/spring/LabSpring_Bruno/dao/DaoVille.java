package fr.afpa.lab.spring.LabSpring_Bruno.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.afpa.lab.spring.LabSpring_Bruno.beans.Ville;

@Repository
public class DaoVille {

	List<Ville> listeVille = new ArrayList<Ville>();
	Ville toulouse = new Ville();
	Ville bordeaux = new Ville();
	Ville paris = new Ville();

	public List<Ville> getListeVille() {
		listeVille.add(toulouse);
		listeVille.add(bordeaux);
		listeVille.add(paris);
		
		toulouse.setId(1);
		toulouse.setNom("Toulouse");

		bordeaux.setId(2);
		bordeaux.setNom("Bordeaux");
		
		paris.setId(3);
		paris.setNom("Paris");
		
		return listeVille;
	}
	
}
